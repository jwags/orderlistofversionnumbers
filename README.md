## Order a list of version numbers

Given a list of un-ordered version numbers, return a list of them ordered.

Example:

Input: 1.0.1, 1.0, 1, 3.2, 2.4, 3.1, 2.2

Output: 1, 1.0, 1.0.1, 2.2, 2.4, 3.1, 3.2