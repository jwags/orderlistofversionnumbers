def main(l):
    # StackOverflow: https://stackoverflow.com/questions/2574080/sorting-a-list-of-dot-separated-numbers-like-software-versions
    l.sort(key=lambda s: map(int, s.split('.')))
    return l
def main1(l):
    # Mine
    allVersions = []
    for currentVersion in l:
        # Generate a 2d list of all versions. Split on '.' character. Makes it easier
        # to access major version, minor version, and revision version later on.
        versionList = currentVersion.split('.')
        allVersions = allVersions + [versionList]

    # Sort the three lists we have from last for loop
    allVersions.sort(key = orderOnFirstElement)
    
    # 1. Starting with lowest Major version in the provided list of versions, loop
    #       through l and make a list of all versions with the same major version.
    # 2. From that list, loop through starting at the lowest possible minor version
    # 3. Finding all matching versions with the same minor version.
    # 4. Order that list by the revision number and add that ordered list to a more
    #       global ordered list.
    # 5. Increment minor version and repeat steps 2, 3, 4, and 5 until max minor version
    #       for that major version is reached.
    # 6. Increment major version and repeat steps 1, 2, 3, 4, and 5 until max major
    #       version is reached
    allVersionsOrdered = []
    lastMajorVersion = -1
    for currentMajorVersion in allVersions:
        if int(currentMajorVersion[0]) > lastMajorVersion:
            lastMajorVersion = int(currentMajorVersion[0])
            allVersionsWithCurrentMajorVersion = []
            for currentVersion in allVersions:
                if currentVersion[0] == currentMajorVersion[0]:
                    allVersionsWithCurrentMajorVersion = allVersionsWithCurrentMajorVersion + [currentVersion]
            allVersionsWithCurrentMajorVersion.sort(key = orderOnSecondElement)
            
            lastMinorVersion = -2
            for currentMinorVersion in allVersionsWithCurrentMajorVersion:
                allVersionsWithCurrentMinorVersion = []
                if len(currentMinorVersion) == 1 or (len(currentMinorVersion) > 1 and int(currentMinorVersion[1]) > lastMinorVersion):
                    if len(currentMinorVersion) == 1 and lastMinorVersion < -1:
                        lastMinorVersion = -1
                    if len(currentMinorVersion) > 1 and int(currentMinorVersion[1]) >= lastMinorVersion:
                        lastMinorVersion = int(currentMinorVersion[1])
                    for currentVersion in allVersionsWithCurrentMajorVersion:
                        if len(currentVersion) == 1 and lastMinorVersion == -1:
                            allVersionsWithCurrentMinorVersion = allVersionsWithCurrentMinorVersion + [currentVersion]
                        if len(currentVersion) > 1 and lastMinorVersion == int(currentVersion[1]):
                            allVersionsWithCurrentMinorVersion = allVersionsWithCurrentMinorVersion + [currentVersion]
                            
                allVersionsWithCurrentMinorVersion.sort(key = orderOnThirdElement)
                allVersionsOrdered = allVersionsOrdered + allVersionsWithCurrentMinorVersion

    # Make list of all versions ordered into a list of strings with the major, minor,
    # and revision numbers concatenated, seperated by '.'
    solution = []
    for currentVersion in allVersionsOrdered:
        currentVersionAsString = currentVersion[0]
        if len(currentVersion) > 1:
           currentVersionAsString = currentVersionAsString + '.' + currentVersion[1]
        if len(currentVersion) > 2:
           currentVersionAsString = currentVersionAsString + '.' + currentVersion[2]
        solution = solution + [currentVersionAsString]
    return solution

def orderOnFirstElement(elem):
    return int(elem[0])
def orderOnSecondElement(elem):
    if len(elem) > 1:
        return int(elem[1])
    return -1
def orderOnThirdElement(elem):
    if len(elem) == 1:
        return -2
    if len(elem) == 2:
        return -1
    return int(elem[2])
def isElementInList(elementToFind, listToSearch):
    for currentElement in listToSearch:
        if elementToFind == currentElement:
            return True
    return False
def test1(methodToRun):
    testData = ["1.11", "2.0.0", "1.2", "2", "0.1", "1.2.1", "1.1.1", "2.0"]
    expected = ["0.1", "1.1.1", "1.2", "1.2.1", "1.11", "2", "2.0", "2.0.0"]
    actual = methodToRun(testData)
    print('Test 1')
    print('Input: ', testData)
    print('Expected: ', expected)
    print('Actual: ', actual)
    print('Passed: ', expected == actual)
    print('\r\n')
def test2(methodToRun):
    testData = ["1.1.2", "1.0", "1.3.3", "1.0.12", "1.0.2"]
    expected = ["1.0", "1.0.2", "1.0.12", "1.1.2", "1.3.3"]
    actual = methodToRun(testData)
    print('Test 2')
    print('Input: ', testData)
    print('Expected: ', expected)
    print('Actual: ', actual)
    print('Passed: ', expected == actual)
    print('\r\n')
def test3(methodToRun):
    testData = ["1.5.3", "1.5", "2", "3.0.1", "1.5.2", "3.1.0", "11", "2.5.5", "2.5.1"]
    expected = ["1.5", "1.5.2", "1.5.3","2", "2.5.1", "2.5.5", "3.0.1", "3.1.0", "11"]
    actual = methodToRun(testData)
    print('Test 3')
    print('Input: ', testData)
    print('Expected: ', expected)
    print('Actual: ', actual)
    print('Passed: ', expected == actual)
    print('\r\n')

print('StackOverflow\'s solution:')
test1(main)
test2(main)
test3(main)

print('My solution:')
test1(main1)
test2(main1)
test3(main1)
